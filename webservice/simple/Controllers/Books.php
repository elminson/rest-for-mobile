<?php
class Controllers_Books extends RestController {
	public function verify($key) {
		if($key=='1234') {
		return true;
		} else { return false; }
		}
	
	public function get() {
		
		$this->response = array('Response' => 'I am GET response. Variables sent are - ' . http_build_query($this->request['params']));
		$vars=$this->request['params'];
		$valid=$this->verify($vars['apikey']);
		if($valid==false){
		$this->response = array('Response' => 'You need a valid apikey'); 	
		$this->responseStatus = 401;
		}else {
		switch ($vars['list']) {
			 case 'books':
			 {
			 	//ESTA DATA PUEDE VENIR DE UNA BASE DE DATOS
				$data=array(array('id'=>1,'name'=>'Book 1','author'=>'Autor 1'),array('id'=>2,'name'=>'Book 2','author'=>'Autor 2'));	
				$this->response = array('Response' => 'Book list','data'=>$data);		 	
			 }
			 case 'mag':
			 {
			 	//ESTA DATA PUEDE VENIR DE UNA BASE DE DATOS
				$data=array(array('id'=>1,'name'=>'Mag 1','author'=>'Autor 1'),array('id'=>2,'name'=>'People 2','author'=>'Autor 2'));	
				$this->response = array('Response' => 'Book list','data'=>$data);		 	
			 }
			 
		$this->responseStatus = 200;
		}
		}
	}
	public function post() {
		$this->response = array('Response' => 'I am POST response. Variables sent are - ' . http_build_query($this->request['params']));
		switch ($var['save']) {
			 case 'books':
			 {
			 $data=array('name'=>$var['name'],'author'=>$var['author']);	
			 $this->response = array('Response' => 'Book saved','data'=>$data);	
			 }
		}
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('Response' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('Response' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 200;
	}
}
