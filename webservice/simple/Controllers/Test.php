<?php
class Controllers_Test extends RestController {
	public function get() {
		$this->response = array('TestResponse' => json_encode($this->request['params']).'I am GET response. Variables sent are - ' . http_build_query($this->request['params']));
		$var=$this->request['params'];
		if($var['list']=='books'){
		//ESTA DATA PUEDE VENIR DE UNA BASE DE DATOS
		$data=array('book1','book2','book3');	
		$this->response = array('Response' => 'Book list','data'=>$data);	
		}
		$this->responseStatus = 200;
	}
	public function post() {
		$this->response = array('TestResponse' => 'I am POST response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 201;
	}
	public function put() {
		$this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 200;
	}
	public function delete() {
		$this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 200;
	}
}
