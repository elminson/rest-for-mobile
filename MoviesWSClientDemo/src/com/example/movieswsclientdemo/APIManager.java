package com.example.movieswsclientdemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class APIManager {
    private HttpClient client = new DefaultHttpClient();
    private static APIManager instance;
	private APIManager(){
		
	}
	public static APIManager getInstance(){
		if(instance==null){
			instance=new APIManager();
		}
		return instance;
	}
	public void checkUrl(String url){
		  try {
	        	InetAddress address = InetAddress.getByName(url);
	 
	        	} catch (UnknownHostException e) {
	        	e.printStackTrace();
	        	}catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	public String processResponse(BufferedReader reader) throws IOException{
		StringBuilder builder = new StringBuilder();
	   String line;
		while ((line = reader.readLine()) != null) {
            builder.append(line);
          }
		 return builder.toString();
	}
	
    public String get(String url){
    	
    	checkUrl(url);
    	String retVal="";
    	
        HttpGet httpGet = new HttpGet(url);
        
        try {
          HttpResponse response = client.execute(httpGet);
          StatusLine statusLine = response.getStatusLine();
          int statusCode = statusLine.getStatusCode();
          if (statusCode == 200) {
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(content));
       
           retVal= processResponse(reader);
          
          } else {
            Log.e("Error", "Failed to download file");
          }
        } catch (ClientProtocolException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
       return retVal;
   }
}
