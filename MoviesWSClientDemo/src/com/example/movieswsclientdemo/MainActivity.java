package com.example.movieswsclientdemo;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Build;

public class MainActivity extends ActionBarActivity {
    private String url="http://server.rlist.net/webservice/simple/movies?list=movies&limit=2&apikey=1234";
    private ArrayAdapter<Movie> adapter;
    private ArrayList<Movie> movies=new ArrayList<Movie>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	    new MoviceAsyncTask().execute("1","2","3");
		ListView listView=(ListView)findViewById(R.id.listView1);
		adapter=new ArrayAdapter<Movie>(this,android.R.layout.simple_list_item_1,movies);
		listView.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	private class MoviceAsyncTask extends AsyncTask<String,Void,ArrayList<Movie>>{

		@Override
		protected ArrayList<Movie> doInBackground(String... params) {
			// TODO Auto-generated method stub
			ArrayList<Movie> movies=new ArrayList<Movie>();
			
			String result=APIManager.getInstance().get(url);
			
			JSONObject obj;
			try {
				obj = new JSONObject(result);

				JSONArray array=obj.getJSONArray("data");
				
				Log.d("RESPONSE",result);
				Log.d("PARAMS",String.format("{0} - {1} - {2}",params[0],params[1],params[2]));
				for(int i=0;i<array.length();i++){
					JSONObject mObj=array.getJSONObject(i);
					
					movies.add(new Movie(mObj.getString("id"),mObj.getString("name"),mObj.getString("author")));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			return movies;
		}
		
		@Override
		protected void onPostExecute(ArrayList<Movie> result) {
        	movies.clear();
            movies.addAll(result);
             adapter.notifyDataSetChanged();
        }
    	
		
	}



}
